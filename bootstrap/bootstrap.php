<?php
error_reporting(E_ALL);

ini_set('display_errors', true);

ini_set('error_reporting',  E_ALL);

require_once __DIR__."/../vendor/autoload.php";

// Директория с конфигами
$config_dir = __DIR__."/../config/";

$container = new Pimple();

// Получим главный конфиг программы
$container['main_config'] = Spyc::YAMLLoad($config_dir.'main.yml');

// Получаем конфиги mongo
$container['mdb_config'] =  Spyc::YAMLLoad($config_dir.'mdb.yml');

// Подключение кmongo
$container['mdb'] = function ($container) {
    $config = $container['mdb_config'];

    try{
        // Пытаемся подключиться mongodb
        $mdb = new MongoClient("mongodb://{$config['user']}:{$config['password']}@{$config['server']}:{$config['port']}/{$config['db']}");
        return $mdb;
    }catch(\Exception $e){
        // Если подключиться не удалось отправлеям сообщение о критической ошибке
        // $app['logger']->write($e->getMessage(), "CRITICAL");
        echo $e;
    };
};



