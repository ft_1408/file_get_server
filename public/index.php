<?php
require_once __DIR__.'/../bootstrap/bootstrap.php';

$app = new Silex\Application();
$app['debug'] = true;
$app['config'] = $container;

// !!!! Пасхальное яичко !!!!!!!!
$app->get('/', function () {

    return '!';
});

$app->get('/get', function (Silex\Application $app) {

    // Получаем данные запроса
    $uid = (int)$app['request']->get('uid');
    $fid = $app['request']->get('fid');

    if(isset($uid) && isset($fid)){
        $mId = new MongoId($fid);

        // Получаем конфиг
        $mdb = $app['config']['mdb'];

        // Получаем информацию о запрошенном файле
        $file = $mdb->flash->user->files->findOne(array('_id'=>$mId, 'user_id'=>$uid));

        if(empty($file)){
            return $app->abort(404, 'File not found.');
        }
        // Если файл сществует
        if(file_exists($app['config']['main_config']['file_dir'] . "/" . $file['machine_fileName'])){

            // Формируем путь к файлу
            $path = $app['config']['main_config']['file_dir'] . "/" . $file['machine_fileName'];

            // Получим расширение файла
            $extension = strrchr($file['user_fileName'], '.');

            // Получим имя файла
            $fileName = Utils::safeTranslit(str_replace($extension, "", $file['user_fileName']));


            // Отправляем файл с именем, заданным пользователем
            return $app
                ->sendFile($path)
                ->setContentDisposition(Symfony\Component\HttpFoundation\ResponseHeaderBag::DISPOSITION_ATTACHMENT,  $fileName);
        };

    }

    return ";-p" ;

});
$app->run();
