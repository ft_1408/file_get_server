# What the hell is this?
This is server for downloading files, using amqp protocol.
Why? Because it`s looking and sounds super cool :)

# Requiroments
- php 5.4 - 5.6
- composer
- rabbitmq server
- php-bcmath
- php-xsl
- php-xml
- php-mcrypt
- mongodb

# Exsample of vhost configuration

    <VirtualHost *:80>
        ServerName file.flash.local.com

        ServerAdmin webmaster@localhost
        DocumentRoot /flash_data/file_get_server/public

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        <Directory /flash_data/file_get_server/public/>
                 DirectoryIndex index.php
                AllowOverride All
                Require all granted
                Order allow,deny
                Allow from all
        </Directory>
    </VirtualHost>
    # vim: syntax=apache ts=4 sw=4 sts=4 sr noet
